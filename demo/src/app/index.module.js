(function() {
  'use strict';

  /**
   * Main module of the Fuse
   */
  angular
    .module('rapi', [
      
      'fuse',

      'app.calendar',
      'app.scrumboard',

      //RAPI FUSE
      'rapi.fuse',

      //Plugins RAPI
      'rapi.companies',

      // Sample
      'main.sample'
    ]);
})();
