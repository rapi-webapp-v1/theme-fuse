(function ()
{
    'use strict';

    angular
        .module('rapi')
        .config(config);

    /** @ngInject */
    function config($translateProvider, $translatePartialLoaderProvider, w3ConfigProvider)
    {
        // Put your custom configurations themes
        var env = getConfENV();

        w3ConfigProvider.config({
            'welcome' : 'Bem vindo ao SC Divulga',
            'welcome_text' : 'No seu painel do SC Divulga você poderá cadastrar um carro, etc..',
            'logo' : '/admin/assets/images/logos/logo-p.jpg',
            'sigla' : 'SC',
            'bg_auth' : '/admin/assets/images/backgrounds/bg-auth.jpg'
        });

        w3ConfigProvider.configHost({
            'baseUrl'    : env.url,
            'baseUrlApi' : '/api/v1',
            'env'        : env.name
        });

        w3ConfigProvider.configAuth({
            'routeSuccess'   : 'app.sample'
        });
        w3ConfigProvider.configUpload({
            'gallery' : 'all',
            'token' : 'scd@!1234$token'
        });

        $translatePartialLoaderProvider.addPart('app/langs');
        $translateProvider.preferredLanguage('pt-br');
    }

    function getConfENV()
    {
        var host = document.location.host;

        var env = {'url': '', 'name': ''};

        if( host === 'localhost:3000' || host === 'scdivulga.env'){
            env.url  = 'http://scdivulga.env';
            env.name = 'localhost';
        }else if( host === 'dev.scdivulga.com.br' ){
            env.url  = 'http://dev.scdivulga.com.br';
            env.name = 'staging';
        }else if( host === 'www.scdivulga.com.br' ){
            env.url  = 'http://www.scdivulga.com.br';
            env.name = 'production';
        }

        return env;
    }


})();
