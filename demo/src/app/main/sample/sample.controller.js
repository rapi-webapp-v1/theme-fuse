(function ()
{
    'use strict';

    angular
        .module('main.sample')
        .controller('SampleController', SampleController);

    /** @ngInject */
    function SampleController()
    {
        var vm = this;

        // Data
        vm.helloText = 'Rapi teste main.sample';

        // Methods

        //////////
    }
})();
