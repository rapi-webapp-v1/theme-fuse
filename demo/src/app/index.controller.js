(function() {
  'use strict';

  angular
    .module('rapi')
    .controller('IndexController', IndexController) ;

  /** @ngInject */
  function IndexController(fuseTheming, w3Config) {
    var vm = this;

    // Data
    vm.themes = fuseTheming.themes;
    vm.bg_auth = w3Config.get('bg_auth');
    vm.logo = w3Config.get('logo');
    vm.sigla = w3Config.get('sigla');

    //////////
  }

})();
