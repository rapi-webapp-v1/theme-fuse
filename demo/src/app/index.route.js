(function ()
{
    'use strict';

    angular
        .module('rapi')
        .config(routeConfig);

    /** @ngInject */
    function routeConfig($stateProvider, $urlRouterProvider, $locationProvider)
    {
        //$locationProvider.html5Mode(true);

        $urlRouterProvider.otherwise('/sample');


    }

})();
