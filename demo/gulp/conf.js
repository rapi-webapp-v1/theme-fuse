/**
 *  This file contains the variables used in other gulp files
 *  which defines tasks
 *  By design, we only put there very generic config values
 *  which are used in several places to keep good readability
 *  of the tasks
 */

var gutil = require('gulp-util');

/**
 *  The main paths of your project handle these with care
 */
exports.paths = {
    src : 'src',
    dist: 'public',
    tmp : '.tmp',
    e2e : 'e2e'
};

/**
 *  Wiredep is the lib which inject bower dependencies in your project
 *  Mainly used to inject script tags in the index.html but also used
 *  to inject css preprocessor deps and js files in karma
 */
exports.wiredep = {
    directory: 'bower_components'
};



/**
 * Configurações RAPI
 */
exports.rapi = {
    'cdn' : {
        'production' : 'http://assets.valmirbarbosa.com.br/fuse/v1.4.0.1',
        'local'      : 'http://cdn.rapi.env'
    },
    'paths' :{
        'public' : 'public/assets/rapi',//  <==  '../../public/assets/rapi',
        'vendor' : '../..'//   <= '../../vendor/rapi'
    },
    'require-dev' : [
        'plugin-companies',
        'fuse'
    ],
    'langs' : ['pt-br', 'en', 'es']
};



/**
 *  Common implementation for an error handler of a Gulp plugin
 */
exports.errorHandler = function (title)
{
    'use strict';

    return function (err)
    {
        gutil.log(gutil.colors.red('[' + title + ']'), err.toString());
        this.emit('end');
    };
};
