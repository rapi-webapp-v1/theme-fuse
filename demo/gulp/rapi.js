'use strict';

var path = require('path');
var gulp = require('gulp');
var conf = require('./conf');
var gutil = require('gulp-util');
var wiredep = require('wiredep').stream;
var _ = require('lodash');
var browserSync = require('browser-sync');
var $ = require('gulp-load-plugins')();
var del = require('del');
var rapi = init();

function init()
{
  console.log('RAPI INIT CONF :D');

  var srcJs  = [];
  var srcCss = [];
  var srcDist= [];

  //Plugins DEV
  var pdev  = conf.rapi['require-dev'];

  for (var i = 0; i < pdev.length; i++) {

      var vendor     = conf.rapi.paths.vendor;
      var pathPlugin = path.join(vendor, pdev[i] + '/resources');
      var plugin     = require( '../' + pathPlugin + '/config.json');

      var dist       = '/dist/';
      srcDist.push({
          'name' : plugin.name,
          'dir'  : (pathPlugin + dist + 'assets/**/*')
      });

      //js
      for (var c = 0; c < plugin['injectJs'].length; c++) {
          var cp = path.join(pathPlugin, dist +  plugin['injectJs'][c]);
          srcJs.push(cp);
      }

      //css
      for (var cc = 0; cc < plugin['injectCss'].length; cc++) {
          var cp2 = path.join(pathPlugin, dist + plugin['injectCss'][cc]);
          srcCss.push(cp2);
      }
  }

  return {
    'plugins' : pdev,
    'js' : srcJs,
    'css' : srcCss,
    'dist' : srcDist
  };
}

gulp.task('rapi:langs', function ()
{
    var langs = conf.rapi.langs || [];

    langs.map(function (lang) {

        var file = '**/i18n/' + lang + '.json';

        gulp.src([
            path.join(conf.paths.tmp, '/serve/assets/rapi/' + file),
            path.join(conf.paths.src, '/app/main/' + file),
            path.join(conf.paths.src, '/app/rapi/' + file.replace('**/', ''))
        ])
        .pipe($.mergeJson(lang + '.json'))
        .pipe(gulp.dest(path.join(conf.paths.src, '/app/langs/i18n')));
    });
});

gulp.task('rapi:list', function ()
{
    var log = function(k){
      console.log('* ' + k);
    };

    var row = function(){
      console.log("    ");
    };

    gutil.log(gutil.colors.blue('[Rapi-plugins]'));
    rapi.plugins.map(log);
    row();

    gutil.log(gutil.colors.green('[STYLES]'));
    rapi.css.map(log);
    row();

    gutil.log(gutil.colors.green('[SCRIPTS]'));
    rapi.js.map(log);
    row();

    gutil.log(gutil.colors.green('[DIST]'));
    rapi.dist.map(function(d){
        console.log('* ['+d.name+'] ' + d.dir);
    });
    row();
});

gulp.task('rapi:watch', ['rapi:langs', 'rapi:other-tmp', 'rapi:scripts', 'rapi:styles'], function ()
{
    var langSrc = path.join(conf.paths.src, '/app/main/**/i18n/*.json');

    gulp.watch(langSrc, ['rapi:langs', 'inject-reload']);
    gulp.watch(rapi.css, ['rapi:styles', 'inject-reload']);
    gulp.watch(rapi.js, ['rapi:scripts', 'inject-reload']);
});

gulp.task('rapi:styles', function ()
{
    return gulp.src(rapi.css)
               .pipe($.size())
               .pipe(gulp.dest(path.join(conf.paths.tmp, '/serve/app/rapi')));
});


gulp.task('rapi:scripts', function ()
{
    return gulp.src(rapi.js)
               .pipe($.size())
               .pipe(gulp.dest(path.join(conf.paths.tmp, '/serve/app/rapi')));
});

var fileFilter = $.filter(function (file)
{
    return file.stat.isFile();
});

function makebuildOther(list, path)
{
    var src = list.map(function(l){
        return l.dir;
    });

    return gulp.src(src)
                .pipe(fileFilter)
                .pipe(gulp.dest(path));

}

gulp.task('rapi:other', ['rapi:clean'], function ()
{
   return makebuildOther(rapi.dist, conf.rapi.paths.public);
});

gulp.task('rapi:other-tmp', function ()
{
    return makebuildOther(rapi.dist, path.join(conf.paths.tmp, '/serve/assets/rapi/'));
});


gulp.task('rapi:clean', function ()
{
    return del.sync([
        conf.rapi.paths.public,
        path.join(conf.paths.tmp, '/serve/assets/rapi/'),
        path.join(conf.paths.tmp, '/serve/app/rapi/')
    ], {force:true});
});
