# 1.4.0 inicio (2016-06-08)

## Install
- "Autolinker.js": "^0.26.0"
- "ng-file-upload-shim": "^12.0.4"
- "ng-file-upload": "^12.0.4"
- "angular-toastr": "^1.7.0"

## Bug Fixes
- **$browser:** texto marcacao de exemplo

## Features
- **$browser:** texto marcacao de exemplo

## Performance Improvements
- **$browser:** texto marcacao de exemplo
