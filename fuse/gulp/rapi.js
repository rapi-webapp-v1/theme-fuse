'use strict';

var path = require('path');
var gulp = require('gulp');
var conf = require('./conf');
//var concat = require('gulp-concat');

var $ = require('gulp-load-plugins')({
    pattern: ['gulp-*', 'main-bower-files', 'uglify-save-license', 'del']
});

/*
.pipe(jsFilter)
//.pipe($.sourcemaps.init())
.pipe($.ngAnnotate())
//.pipe($.uglify({preserveComments: $.uglifySaveLicense})).on('error', conf.errorHandler('Uglify'))
// .pipe($.rev())
//.pipe($.sourcemaps.write('maps'))
.pipe(jsFilter.restore)
.pipe(cssFilter)


//.pipe($.sourcemaps.init())
.pipe($.cleanCss())
// .pipe($.rev())
//.pipe($.sourcemaps.write('maps'))
.pipe(cssFilter.restore)
*/

gulp.task('rapi:concatjs', function() {

    var js = [
        path.join(conf.paths.dist, '/scripts/vendor.js'),
        path.join(conf.paths.dist, '/scripts/app.js')
    ];

    return gulp.src(js)
    .pipe($.sourcemaps.init())
    .pipe($.concat('rapi-fuse.js'))
    .pipe($.uglify({preserveComments: $.uglifySaveLicense})).on('error', conf.errorHandler('Uglify'))
    .pipe($.sourcemaps.write('maps'))
    .pipe(gulp.dest(path.join(conf.paths.dist, '/')));
});

gulp.task('rapi:concatjs-dev', function() {

    var js = [
        path.join(conf.paths.dist, '/scripts/vendor.js'),
        path.join(conf.paths.dist, '/scripts/app.js')
    ];

    return gulp.src(js)
    .pipe($.concat('rapi-fuse.js'))
    .pipe(gulp.dest(path.join(conf.paths.dist, '/')));
});

gulp.task('rapi:concatcss', function() {

    var css = [
        path.join(conf.paths.dist, '/styles/vendor.css'),
        path.join(conf.paths.dist, '/styles/app.css')
    ];

    return gulp.src(css)
    .pipe($.sourcemaps.init())
    .pipe($.concat('rapi-fuse.css'))
    .pipe($.cleanCss())
    .pipe($.sourcemaps.write('maps'))
    .pipe(gulp.dest(path.join(conf.paths.dist, '/')));
});

gulp.task('rapi:concatcss-dev', function() {

    var css = [
        path.join(conf.paths.dist, '/styles/vendor.css'),
        path.join(conf.paths.dist, '/styles/app.css')
    ];

    return gulp.src(css)
    .pipe($.concat('rapi-fuse.css'))        
    .pipe(gulp.dest(path.join(conf.paths.dist, '/')));
});

gulp.task('rapi:fuse', ['rapi:concatjs', 'rapi:concatcss']);
//
// gulp.task('rapi:js', function() {
//
// 	var jsFilter = $.filter('**/*.js', {restore: true});
// 	//gulp.src(path.join(conf.paths.src, '/app/**/*.js'))
// 	// .pipe($.size())
// 	//
//
//     var injectScripts = [
//             path.join(conf.paths.src, '/app/**/*.module.js'),
//             path.join(conf.paths.src, '/app/**/*.js'),
//             path.join('!' + conf.paths.src, '/app/**/*.spec.js'),
//             path.join('!' + conf.paths.src, '/app/**/*.mock.js'),
//         ];
//
// 	gulp.src($.mainBowerFiles(jsFilter).concat(injectScripts))
// 		.pipe(jsFilter)
// 		.pipe($.flatten())
// 		.pipe($.sourcemaps.init())
// 		.pipe($.ngAnnotate())
// 		//.pipe($.uglify({preserveComments: $.uglifySaveLicense})).on('error', conf.errorHandler('Uglify'))
// 		// .pipe($.rev())
// 		.pipe($.concat('rapi-fuse.js'))
// 		.pipe($.sourcemaps.write('maps'))
// 		.pipe(jsFilter.restore)
// 		.pipe(gulp.dest(path.join(conf.paths.dist, '/')));
//         // .pipe($.size({
//         //     title    : path.join(conf.paths.dist, '/'),
//         //     showFiles: true
//         // }));
// });
//
// // Only applies for fonts from bower dependencies
// // Custom fonts are handled by the "other" task
// gulp.task('rapi:fonts', function ()
// {
//     return gulp.src($.mainBowerFiles())
//         .pipe($.filter('**/*.{eot,svg,ttf,woff,woff2}'))
//         .pipe($.flatten())
//         .pipe(gulp.dest(path.join(conf.paths.dist, '/fonts/')));
// });
//
// gulp.task('rapi:other', function ()
// {
//     var fileFilter = $.filter(function (file)
//     {
//         return file.stat.isFile();
//     });
//
//     return gulp.src([
//             path.join(conf.paths.src, '/**/*'),
//             path.join('!' + conf.paths.src, '/**/*.{html,css,js,scss}')
//         ])
//         .pipe(fileFilter)
//         .pipe(gulp.dest(path.join(conf.paths.dist, '/')));
// });
//
// gulp.task('rapi:build', ['rapi:fonts', 'rapi:other', 'rapi:assets']);
