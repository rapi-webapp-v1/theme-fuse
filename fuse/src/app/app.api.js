(function ()
{
    'use strict';

    angular
        .module('fuse')
        .factory('api', apiService);

    /** @ngInject */
    function apiService($resource)
    {

        var api = {};

        // Base Url
        api.baseUrl = 'app/data/';

        return api;
    }

})();
