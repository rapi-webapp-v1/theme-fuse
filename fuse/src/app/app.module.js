(function ()
{
    'use strict';

    angular
        .module('fuse',
            [
                'app.core'
            ]);
})();
